<?php

namespace app\assets;

use yii\web\AssetBundle;


class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/template/startbootstrap/css/styles.css',
    ];
    public $js = [
        '/template/startbootstrap/js/scripts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
      // 'yii\bootstrap\BootstrapAsset',
    ];
}

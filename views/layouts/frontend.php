<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAsset;


FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    
    <title>FANDI AJI WIBOWO | <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
        
        <link rel="icon" type="image/x-icon" href="/template/startbootstrap/assets/img/ico.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
     
    <link rel="stylesheet" href="/template/startbootstrap/css/fontawesome.css" />
    <link rel="stylesheet" href="/template/startbootstrap/css/owl.css" />
    <link rel="stylesheet" href="/template/startbootstrap/css/lightbox.css" />
    <link rel="stylesheet" href="/template/startbootstrap/css/templatemo-style.css" />

</head>
<body id="page-top">
    <?php $this->beginBody() ?>

        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
          <i id="menu-toggle"></i>
           <i  id="menu-close"></i>
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-block d-lg-none"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="/template/startbootstrap/assets/img/fd.png" style="width:40px; height:40px; " alt="..." /><span style="margin-left:10px;"></span> Fandi Aji Wibowo</span>
                <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="/template/startbootstrap/assets/img/fd.png" alt="..." /></span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#interests">Interests</a></li>
                    <!-- <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#awards">Awards</a></li> -->
                </ul>
            </div>
        </nav>
        <!-- Page Content-->

        <div class="container-fluid p-0"> -->
             <?= $content ?>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
     
        <script src="/template/startbootstrap/js/isotope.min.js"></script>
        <script src="/template/startbootstrap/js/owl-carousel.js"></script>
        <script src="/template/startbootstrap/js/lightbox.js"></script>
        <script src="/template/startbootstrap/js/custom.js"></script>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

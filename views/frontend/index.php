<?php 
  use app\widgets\layout\aboutWidget;
  use app\widgets\layout\experienceWidget;
  use app\widgets\layout\educationWidget;
  use app\widgets\layout\interestsWidget;
  use app\widgets\layout\sklillsWidget;
  use app\widgets\layout\awardsWidget;
  use app\widgets\layout\potrofolioWidget;
  
?>
    <!-- About-->
    <section class="resume-section" id="about">
        <?= aboutWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Experience-->
    <section class="resume-section" id="experience">
        <?= experienceWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Education-->
    <section class="resume-section" id="education">
        <?= educationWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Skills-->
    <section class="resume-section" id="skills">
        <?= sklillsWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Skills-->
    <section class="resume-section" id="portofolio">
        <?= potrofolioWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Interests-->
    <section class="resume-section" id="interests">
        <?= interestsWidget::widget() ?>
    </section>
    <hr class="m-0" />

    <!-- Awards-->
    <!-- <section class="resume-section" id="awards">
        ?= awardsWidget::widget() ?>
    </section> -->
<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Education';
?>  

<div class="resume-section-content">
    <h2 class="mb-5">Education</h2>
    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">Yogyakarta University of Technology</h3>
            <div class="subheading mb-3">Diploma D3</div>
            <div>Informatics Management</div>
            <p>GPA: 3.02</p>
        </div>
        <div class="flex-shrink-0"><span class="text-primary">September 2011 - April 2015</span></div>
    </div>

    <!-- <div class="d-flex flex-column flex-md-row justify-content-between">
        <div class="flex-grow-1">
            <h3 class="mb-0">SMK PN2 PURWOREJO</h3>
            <div class="subheading mb-3">Teknik Otomotif</div>
            <p>Passing grade Nilai: 7.0</p>
        </div>
        <div class="flex-shrink-0"><span class="text-primary">August 2002 - May 2006</span></div>
    </div> -->
</div>
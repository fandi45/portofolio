<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Experience';
?>
<style>
  .data1{
      margin-bottom: -1px;

  }
</style>
<div class="resume-section-content">
    <h2 class="mb-5">Portofolio</h2>
    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">My Work</h3>
            <p> Here's the work I've ever made :</p>
            
             <!-- //// -->

            <div class="row">
            <div class="isotope-wrapper">
              <form class="isotope-toolbar">
                <label
                  ><input
                    type="radio"
                    data-type="*"
                    checked=""
                    name="isotope-filter"
                  />
                  <span>all</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="website"
                    name="isotope-filter"
                  />
                  <span>Website</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="desktop"
                    name="isotope-filter"
                  />
                  <span>Desktop</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="android"
                    name="isotope-filter"
                  />
                  <span>Android/mobile</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="design"
                    name="isotope-filter"
                  />
                  <span>Design (Logo/Banner)</span></label
                >
              </form>

              <div class="isotope-box">

               <div class="isotope-item" data-type="desktop">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/simase.png"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/simase.png"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>SIMASE</h4>
                      <span>Sistem Manajemen Sekolah</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="website">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/psdm.PNG"
                      alt="PSDM"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/psdm.PNG"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>E-learning app</h4>
                      <span>Used in PSDM West Java Provincial Mental Hospital</span>
                    </figcaption>
                  </figure>
                </div>
                
                <div class="isotope-item" data-type="website">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/mypos.PNG"
                      alt="PSDM"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/mypos.PNG"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>My POS</h4>
                      <span>mypos kasir and sales</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="website">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/exomerce.PNG"
                      alt="PSDM"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/exomerce.PNG"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>HASFA SHOP</h4>
                      <span>E-commerce</span>
                    </figcaption>
                  </figure>
                </div>


                <div class="isotope-item" data-type="website">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/simpeda.png"
                      alt="SIMPEDA"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/simpeda.png"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>SIMPEDA</h4>
                      <span>Used in SIMPEDA in yayasan</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="website">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/koperasi.PNG"
                      alt="Koperasi Sedekah"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/koperasi.PNG"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Koperasi Sedekah</h4>
                      <span>In process ...</span>
                    </figcaption>
                  </figure>
                </div>
            
                <!-- <div class="isotope-item" data-type="android">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/portfolio-03.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/portfolio-03.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Item Third</h4>
                      <span>customize anything</span>
                    </figcaption>
                  </figure>
                </div> -->

                <div class="isotope-item" data-type="design">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/Smart-energi.png"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/Smart-energi.png"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      
                      <h4>Logo Sinergi</h4>
                      <span>Your Company Name</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="design">
                  <figure class="snip1321">
                    <img
                      src="/template/startbootstrap/assets/img/banner1.png"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="/template/startbootstrap/assets/img/banner1.png"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      
                      <h4>Banner Food</h4>
                      <span>Healthy food</span>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
          </div>

             <!-- ///// -->

        </div>
        <div class="flex-shrink-0"><span class="text-primary">
          <!-- ...Present -->
        </span></div>
    </div>

</div>
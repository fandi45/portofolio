<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Skills';
?>

<div class="resume-section-content">
<h2 class="mb-5">Skills</h2>
<div class="subheading mb-3">Programming Languages & Tools</div>
<ul class="list-inline dev-icons">
    <li class="list-inline-item"><i class="fab fa-html5" title="Html5"></i></li>
    <li class="list-inline-item"><i class="fab fa-js-square"></i></li>
    <li class="list-inline-item"><i class="fab fa-angular"></i></li>
    <li class="list-inline-item"><i class="fab fa-react"></i></li>
    <li class="list-inline-item"><i class="fab fa-node-js"></i></li>
    <li class="list-inline-item"><i class="fab fa-wordpress"></i></li>
    <li class="list-inline-item"><i class="fab fa-npm"></i></li>
</ul>
<h5>Yii2 Framework</h5>
<div class="progress progress col-md-7" >
	<div class="progress-bar" role="progressbar" style="width: 95%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="100" aria-valuemax="0"><strong>95%</strong></div>
</div>
 
<br/>
<h5>Boostrape 4</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 95%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>95%</strong></div>
</div>

<br/>
<h5>PHP 7.3</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 95%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>95%</strong></div>
</div>

<br/>
<h5>Mysql</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 85%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>85%</strong></div>
</div>

<br/>
<h5>Mongo DB</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 65%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>65%</strong></div>
</div>

<br/>
<h5>CI Framework</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 85%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>85%</strong></div>
</div>

<br/>
<h5>Delphi 6 / Embarcadero</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 85%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>85%</strong></div>
</div>

<br/>
<h5>IBExpert</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 85%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>85%</strong></div>
</div>

<br/>
<h5>Docker</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 75%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>75%</strong></div>
</div>

<br/>
<h5>Adobe Ilustrator</h5>
<div class="progress col-md-7">
	<div class="progress-bar" role="progressbar" style="width: 75%; background-color:#38bd4c" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"> <strong>75%</strong></div>
</div>

<br>
<div class="subheading mb-3">Workflow</div>
<ul class="fa-ul mb-0">
    <li>
        <span class="fa-li"><i class="fas fa-check"></i></span>
        Mobile-First, Responsive Design
    </li>
    <br>
    <li>
        <span class="fa-li"><i class="fas fa-check"></i></span>
        Cross Browser Testing & Debugging
    </li>
    <br>
    <li>
        <span class="fa-li"><i class="fas fa-check"></i></span>
        Cross Functional Teams
    </li>
  
</ul>
</div>
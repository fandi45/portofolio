<?php

        return [
            
            /* ===== Backend ===== */
           
            '/'                                           => '/frontend/index',
            '/login'                                      => '/login',
            '/register'                                   => '/register',
            '/logout'                                     => '/login/logout',
            
            // MENU //
            '/dashboard/rbac/menu'                        => '/dashboard/rbac/menu/index',
            '/dashboard/overview'                         => '/dashboard/overview/default/index',
            '/dashboard/barang/barang'                    => '/dashboard/barang/barang/index',
            '/dashboard/pembeli/pembeli'                  => '/dashboard/pembeli/pembeli/index',
       
        ];

?>
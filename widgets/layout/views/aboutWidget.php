<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'About Me';
?>
<div class="resume-section-content">
  <h1 class="mb-2" >
      Fandi Aji
      <span class="text-primary">Wibowo</span>
  </h1>
  <div class="subheading mb-5">
        (Software Engineer) Adress: Ngetos Kulon Sriwedari· Muntilan, Magelang,56415 ·  <a></a>
      <a href="#">fndwbw688@gmail.com</a>,  WA: <a href="https://api.whatsapp.com/send?phone=6281214335963&text=Saya%20tertarik%20untuk%20berkerjasama%20dengan%20anda." target="_blank"><img src="/template/startbootstrap/assets/img/wa.png" style="width:40px; height:40px"></a>
  </div>
  <p class="lead mb-5">
  My name is Fandi Aji Wibowo and I am a Software Engineer and graphic designer based in Indonesia. My work experience in an application development company has more than 5 years of programming and part-time graphic designer.
  </p>
  <div class="social-icons">
      <a class="social-icon" href="https://www.linkedin.com/in/fandi-aji-wibowo-493b0462/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
      <a class="social-icon" href="#!"><i class="fab fa-github" target="_blank"></i></a>
      <a class="social-icon" href="https://twitter.com/FandiAjiWibowo1" target="_blank"><i class="fab fa-twitter"></i></a>
      <a class="social-icon" href="https://web.facebook.com/fandi.wibowo2/" target="_blank"><i class="fab fa-facebook-f"></i></a>
  </div>
</div>
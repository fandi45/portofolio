<?php 
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Experience';
?>
<style>
  .data1{
      margin-bottom: -1px;

  }
</style>
<div class="resume-section-content">
    <h2 class="mb-5">Experience XXX</h2>
    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">Web Developer</h3>
            <div class="subheading mb-3">PT.SPE SOLUTION  <span><img src="/template/startbootstrap/assets/img/spe.png" style="width:30px; height:20px"></div> 
            <p> Description :</p>
            
            <p> - QRIS is used to process payment transactions via barcodes in collaboration with PT. National Electronic Transaction Settlement (PTEN), 
                and is used for merchant or store registration, 
                which then gets QRIS as a tool for transactions used by buyers. </p>
            <p> - Insurance landing page as an insurance medium for driving accidents. </p>
            <p>Responsible:</p>

            <p class="data1"> - Creation and development of applications for client needs </p>
            <p class="data1"> - Data analysis according to flow document </p>
            <p> - Maintenance and repair of applications when experiencing problems. </p>

            <p >Technology Used :</p>
            <p class="data1">- GIT, SVN/</p>
            <p class="data1">- PHP 7.3</p>
            <p class="data1">- Vue.js</p>
            <p class="data1">- JavaScript</p>
            <p class="data1">- Yii 2 Framework</p>
            <p class="data1">- MySQL, SQLite DBMS, Mongo DB</p>
            <p class="data1">- GitLab</p>
            <p>- Docker</p>
            
        </div>
        <div class="flex-shrink-0"><span class="text-primary">
            
            <!-- Juni 2019 - Present -->
        </span></div>
    </div>

    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">Desktop Programmer</h3>
            <div class="subheading mb-3">PT.GLOBAL INTERMEDIA NUSANTARA</div>
            <p> Description :</p>
            
            <p> - Development Planning Application (SIMPPD) to handle the process of 
                 determining programs and activities which will be realized at the beginning by the government, 
                 for the welfare of the community</p>
            <p> - SIMDA finance to handle the process of funding the realization of government programs and activities. </p>
            <p>Responsible:</p>

            <p class="data1"> - Perform application maintenance and development.</p>
            <p class="data1"> - Create enduser usage module.</p>
            <p> - Provide assistance to BAPPEDA Planning.</p>

            <p >Projects Involved :</p>
            <p class="data1">- BAPPEDA Palangkaraya (central Kalimantan)</p>
            <p class="data1">- BAPPEDA Murung Raya (central Kalimantan)</p>
            <p class="data1">- BAPPEDA Kapuas (central Kalimantan)</p>
            <p>- Regional Financial and Asset Management Agency (East Waringin City)</p>
 
            <p >Technology Used :</p>
            <p class="data1">- Delphi 6</p>
            <p class="data1">- IBexpert</p>
            <p class="data1">- Git</p>
       
        </div>
        <div class="flex-shrink-0"><span class="text-primary">
            <!-- Juni 2014 - Juni 2016 -->
        </span></div>
    </div>

    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">Freelance Programmer</h3>
            <div class="subheading mb-3">PSDM RSJ Provinsi Jawa Barat</div>
            <p> - Create an e-learning application for employees in the PSDM RSJ field by evaluating 
             workers based on training material tests to be able to follow the practice schedule 
             at the hospital (fullstack developer)
            </p>
            <p>Description:</p>
            <p class="data1">Perform data analysis and create a system based on flow 
                information provided from the PSDM client klien</p>
                <p class="data1">- perform system maintenance</p>
                <p >- provide system assistance to PSDM clients</p>

                <p>Technology used:</p>
                <p class="data1">- Yii2 Framework</p>
                <p class="data1">- PHP 7.2</p>
                <p class="data1">- Vscode Tools</p>
                <p class="data1">- Mysql</p>
                <p class="data1">- Boostarpe 4</p>
                <p class="data1">- Javascript</p>
                <p class="data1">- Git</p>

             </div>
        <div class="flex-shrink-0"><span class="text-primary">

            <!-- Juli 2020 - Present -->
        </span></div>
    </div>
    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
        <div class="flex-grow-1">
            <h3 class="mb-0">Logo Designer</h3>
            <div class="subheading mb-3">Frelance / Partime</div>
            <p> Making a logo according to the client's request by translating the design into the form of a logo/image
            </p>

            <p >Media involved: :</p>
            <p class="data1">- 99Design contest : <a href="https://99designs.com/" target="_blank">https://99designs.com/</a></p>
            <p class="data1">- Sell ​​logo on Logoground : <a href="https://www.logoground.com/" target="_blank">https://www.logoground.com/</a></p>
        </div>
        <div class="flex-shrink-0"><span class="text-primary">

            <!-- Present -->
        </span></div>
    </div>
</div>